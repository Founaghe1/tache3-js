//declaration d'une variable
let maVar; 

//declaration d'une variable + affectation de valeur
let prenom = 'mohamed';
let age = 25;

let choix = null

console.log(prenom +' '+ age +'ans')


//voir le type de variable
console.log('type de age est: ' + typeof age);

console.log('type de choix est: ' + typeof choix);

age = 20;
console.log(`Mon nom est ${prenom}, j'ai ${age} ans`);

//constante 

const MAVILLE = 'Labé';

console.log(MAVILLE)

//Array

let array = ['mangue', 35, false, ['red', 'green', 'blue']];
console.log(array);
//grandeur du tab
console.log(array.length);
//afficher la val de l'index 2
console.log(array[2])
//afficher 
console.log(array[3][1])


//creation d'un objet

let personne = {
    prenom: 'Moha',
    age: 23,
    adresse: {
        rue: '12 rue vincent',
        ville: 'Paris'
    }
}

console.log(personne)
console.log(personne.prenom)
console.log(personne['adresse']['ville'])
console.log(personne.adresse.rue)


//calcul

// let a = 5;
// let b = 3;

// //addition
// console.log(`La somme de a & b est: ${a + b} `);
// //soustraction
// console.log(`La soustraction de a & b est: ${a - b} `);
// //multiplication
// console.log(`La multiplication de a & b est: ${a * b} `);
// //division
// console.log(`La division de a & b est: ${a / b} `);
// //modulo
// console.log(`Le reste de a / b est: ${a % b} `);

// //incrementation
// a++ ;
// a += 1; //recomander en ES6
// console.log(a)

// //decrementer
// b-- ;
// b -= 1; //recomander en ES6
// console.log(b)

//Exo3

let x = "5";
let y = "4";
let z = x + y;

// console.log(`le type de x est: ${typeof x}`);
// console.log(`la valeur de x est: ${x}`);

// console.log(`le type de y est: ${typeof y}`);
// console.log(`la valeur de y est: ${y}`);

// console.log(`le type de z est: ${typeof z}`);
// console.log(`la valeur de z est: ${z}`);

//conversion en entier
let a = parseInt(x);
let b = Number(y);

z = a + b;

console.log(`le type de x est: ${typeof a}`);
console.log(`la valeur de x est: ${a}`);

console.log(`le type de y est: ${typeof b}`);
console.log(`la valeur de y est: ${b}`);

console.log(`le type de z est: ${typeof z}`);
console.log(`la valeur de z est: ${z}`);
